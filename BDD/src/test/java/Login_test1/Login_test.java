package Login_test1;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class Login_test {
WebDriver Speedo=new FirefoxDriver();

@Given("^i am in the new tours login page \"(.*?)\"$")
public void i_am_in_the_new_tours_login_page(String URL) throws Throwable {
    // Write code here that turns the phrase above into concrete actions
    //throw new PendingException();
	Speedo.get(URL);
}

@When("^i enter \"(.*?)\" and \"(.*?)\"$")
public void i_enter_and(String userName, String password) throws Throwable {
    // Write code here that turns the phrase above into concrete actions
    //throw new PendingException();
	Speedo.findElement(By.name("userName")).sendKeys(userName);
	Speedo.findElement(By.name("password")).sendKeys(password);
}

@When("^i click \"(.*?)\" button$")
public void i_click_button(String login) throws Throwable {
    // Write code here that turns the phrase above into concrete actions
    //throw new PendingException();
	Speedo.findElement(By.name("login")).click();
}

@Then("^\"(.*?)\" should be seen$")
public void should_be_seen(String Link) throws Throwable {
    // Write code here that turns the phrase above into concrete actions
    //throw new PendingException();
	Assert.assertTrue(Speedo.findElement(By.linkText(Link)).isDisplayed());
}
}
