/*package features;

import static org.junit.Assert.assertTrue;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import junit.framework.Assert;

public class Login_test {
	WebDriver Speedo =new FirefoxDriver();
	@Given("^i am in the new tours login page \"(.*?)\"$")
	public void i_am_in_the_new_tours_login_page(String URL) throws Throwable {
		Speedo.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
	    // Write code here that turns the phrase above into concrete actions
	    //throw new PendingException();
		Speedo.get(URL);
	}

	@When("^i enter \"(.*?)\" and \"(.*?)\"$")
	public void i_enter_and(String userName, String password) throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	   // throw new PendingException();
		Speedo.findElement(By.name("userName")).sendKeys(userName);
		Speedo.findElement(By.name("password")).sendKeys(password);
		Speedo.findElement(By.name("login")).click();
	}

	@SuppressWarnings("deprecation")
	@Then("^\"(.*?)\" should be seen$")
	public void should_be_seen(String link) throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	    //throw new PendingException();
		Speedo.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		Assert.assertTrue(Speedo.findElement(By.linkText(link)).isDisplayed());
		
	}


}
*/