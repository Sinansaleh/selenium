package newToursDefenition;

import java.util.concurrent.TimeUnit;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

import cucumber.api.DataTable;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class Registrtion2 {
	WebDriver driver= new FirefoxDriver();
	@Given("^i am in NewTour Registration Page \"(.*?)\"$")
	public void i_am_in_NewTour_Registration_Page(String URL) throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	    //throw new PendingException();
		driver.get(URL);
	}
	WebDriver Driver2 = new ChromeDriver();
	@Given("^i am in NewTour Registration Page(\\d+) \"(.*?)\"$")
	public void i_am_in_NewTour_Registration_Page(int arg1, String URL2) throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	    //throw new PendingException();
		Driver2.get(URL2);}

	@When("^I add correct data for all fildes except password and confirm passowrd$")
	public void i_add_correct_data_for_all_fildes_except_password_and_confirm_passowrd(DataTable arg1) throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	    // For automatic transformation, change DataTable to one of
	    // List<YourType>, List<List<E>>, List<Map<K,V>> or Map<K,V>.
	    // E,K,V must be a scalar (String, Integer, Date, enum etc)
	    //throw new PendingException();
		//Firefox
		driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		driver.findElement(By.name("firstName")).sendKeys("sinan");
		driver.findElement(By.name("lastName")).sendKeys("saleh");
		driver.findElement(By.name("phone")).sendKeys("5715289168");
		driver.findElement(By.name("userName")).sendKeys("sinantalawsi@gmail.com");
		driver.findElement(By.name("address1")).sendKeys("20030 coltsfoot terrace");
		driver.findElement(By.name("city")).sendKeys("Ashburn");
		driver.findElement(By.name("state")).sendKeys("VA");
		driver.findElement(By.name("postalCode")).sendKeys("20147");
		driver.findElement(By.name("country")).click();
		driver.findElement(By.name("email")).sendKeys("sinantalawsi@gmail.com");
		//chrom
		Driver2.findElement(By.name("firstName")).sendKeys("sinan");
		Driver2.findElement(By.name("lastName")).sendKeys("saleh");
		Driver2.findElement(By.name("phone")).sendKeys("5715289168");
		Driver2.findElement(By.name("userName")).sendKeys("sinantalawsi@gmail.com");
		Driver2.findElement(By.name("address1")).sendKeys("20030 coltsfoot terrace");
		Driver2.findElement(By.name("city")).sendKeys("Ashburn");
		Driver2.findElement(By.name("state")).sendKeys("VA");
		Driver2.findElement(By.name("postalCode")).sendKeys("20147");
		Driver2.findElement(By.name("country")).click();
		Driver2.findElement(By.name("email")).sendKeys("sinantalawsi@gmail.com");
	}

	@When("^I enter \"(.*?)\" and \"(.*?)\"$")
	public void i_enter_and(String password, String confirmPassword) throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	   // throw new PendingException();
		driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		driver.findElement(By.name("confirmPassword")).sendKeys(confirmPassword);
		driver.findElement(By.name("password")).sendKeys(password);
		Driver2.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		Driver2.findElement(By.name("confirmPassword")).sendKeys(confirmPassword);
		Driver2.findElement(By.name("password")).sendKeys(password);
	}

	@When("^I click \"(.*?)\"$")
	public void i_click(String register) throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	    //throw new PendingException();
		driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		driver.findElement(By.name("register")).click();
		Driver2.findElement(By.name("register")).click();
	}

	@Then("^\"(.*?)\" sholud be seen$")
	public void sholud_be_seen(String Link) throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	    //throw new PendingException();
		try {
			driver.findElement(By.linkText("SIGN-OFF")).click();
			System.out.println("Test Case pass");
			}catch(Exception e) {System.out.println("Test case faild");
			}
			
			try {
				Driver2.findElement(By.linkText("SIGN-OFF")).click();
				System.out.println("Test Case pass");
				}catch(Exception e) {System.out.println("Test case faild");
				}
		driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		Assert.assertTrue(driver.findElement(By.linkText(Link)).isDisplayed());
		driver.close();
		Assert.assertTrue(Driver2.findElement(By.linkText(Link)).isDisplayed());
		Driver2.close();
		
	}
	
	}
	