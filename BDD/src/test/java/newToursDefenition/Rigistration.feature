#Author: your.email@your.domain.com
#Keywords Summary :
#Feature: List of scenarios.
#Scenario: Business rule through list of steps with arguments.
#Given: Some precondition step
#When: Some key actions
#Then: To observe outcomes or validation
#And,But: To enumerate more Given,When,Then steps
#Scenario Outline: List of steps for data-driven as an Examples and <placeholder>
#Examples: Container for s table
#Background: List of steps run before each of the scenarios
#""" (Doc Strings)
#| (Data Tables)
#@ (Tags/Labels):To group Scenarios
#<> (placeholder)
#""
## (Comments)
#Sample Feature Definition Template
@tag
Feature: testing rigistration page with different credintials

  Background: 
    Given i am in NewTour Registration Page "http://newtours.demoaut.com/mercuryregister.php"
    Given i am in NewTour Registration Page1 "http://newtours.demoaut.com/mercuryregister.php"
    When I add correct data for all fildes except password and confirm passowrd
      | firstName | lastName | phone      | userName               | address1                 | city    | state | postalCode | country       | email                  |
      | sinan     | saleh    | 5715289168 | sinantalawsi@gmail.com | 20030 coltsfoot terrrace | Ashburn | VA    |      20147 | UNITED STATES | sinantalawsi@gmail.com |

  Scenario Outline: check with different credintials
    When I enter "<password>" and "<confirmPassword>"
    And I click "<register>"
    Then "<Link>" sholud be seen

    Examples: 
      | password  | confirmPassword | Link     |
      | sinan%123 | sinan%123       | SIGN-OFF |
      | a         | a               | SIGN-ON	 |
      | a         | b               | SIGN-ON	 |
      | b         | a               | SIGN-ON	 |
      |           | a               | SIGN-ON	 |
      | a         |                 | SIGN-ON	 |
      |           |                 | SIGN-ON	 |
