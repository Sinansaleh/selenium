#Author: your.email@your.domain.com
#Keywords Summary :
#Feature: List of scenarios.
#Scenario: Business rule through list of steps with arguments.
#Given: Some precondition step
#When: Some key actions
#Then: To observe outcomes or validation
#And,But: To enumerate more Given,When,Then steps
#Scenario Outline: List of steps for data-driven as an Examples and <placeholder>
#Examples: Container for s table
#Background: List of steps run before each of the scenarios
#""" (Doc Strings)
#| (Data Tables)
#@ (Tags/Labels):To group Scenarios
#<> (placeholder)
#""
## (Comments)
#Sample Feature Definition Template
@tag
Feature: testing new tour rigstration page
 
  Background: testing registration field with correct data
    Given i am in registration Page "http://newtours.demoaut.com/mercuryregister.php"
    When i enter the correct data
    |firstName|lastName|phone|userName|address1|city|state|postalCode|country|email|
    |Saif|Al Jalili|3045022201|saifaj71@gmail.com|20064 Coltsfoot Terrace|Ashburn|VA|20147|UNITED STATES|saifaj71@gmail.com|
    

  
  Scenario Outline: Title of your scenario outline
    When I enter "<password>" and "<confirmPassword>" to check
    And I click "<register>" to submet
    Then  the "<link>" should be seen

    Examples: 
      |password| confirmPassword | link  |
      | a |  a | SIGN-ON |
      | a |  b | SIGN-ON |
      | b |  a | SIGN-ON |
      | b | b | SIGN-ON |
      
