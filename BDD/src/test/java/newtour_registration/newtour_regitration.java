package newtour_registration;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

import cucumber.api.DataTable;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class newtour_regitration {

	WebDriver driver=new FirefoxDriver();

@Given("^i am in registration Page \"(.*?)\"$")
public void i_am_in_registration_Page(String arg1) throws Throwable {
    // Write code here that turns the phrase above into concrete actions
    //throw new PendingException();
	driver.get(arg1);
	
}

@When("^i enter the correct data$")
public void i_enter_the_correct_data(DataTable arg1) throws Throwable {
    // Write code here that turns the phrase above into concrete actions
    // For automatic transformation, change DataTable to one of
    // List<YourType>, List<List<E>>, List<Map<K,V>> or Map<K,V>.
    // E,K,V must be a scalar (String, Integer, Date, enum etc)
    //throw new PendingException();
	driver.findElement(By.name("firstName")).sendKeys("Saif");
	driver.findElement(By.name("lastName")).sendKeys("Al Jalili");
	driver.findElement(By.name("phone")).sendKeys("3045022201");
	driver.findElement(By.name("userName")).sendKeys("saifaj71@gmail.com");
	driver.findElement(By.name("address1")).sendKeys("20064 Coltsfoot Terrace");
	driver.findElement(By.name("city")).sendKeys("Ashburn");
	driver.findElement(By.name("state")).sendKeys("VA");
	driver.findElement(By.name("postalCode")).sendKeys("20147");
	driver.findElement(By.name("country")).click();
	driver.findElement(By.name("email")).sendKeys("saifaj71@gmail.com");
	
	
}
@When("^I enter \"(.*?)\" and \"(.*?)\" to check$")
public void i_enter_and_to_check(String pw, String cpw) throws Throwable {
    // Write code here that turns the phrase above into concrete actions
    //throw new PendingException();
	driver.findElement(By.name("password")).sendKeys(pw);
	driver.findElement(By.name("confirmPassword")).sendKeys(cpw);
	
}

@When("^I click \"(.*?)\" to submet$")
public void i_click_to_submet(String arg1) throws Throwable {
    // Write code here that turns the phrase above into concrete actions
    //throw new PendingException();
	driver.findElement(By.name("register")).click();
	
}

@Then("^the \"(.*?)\" should be seen$")
public void the_should_be_seen(String lin) throws Throwable {
    // Write code here that turns the phrase above into concrete actions
    //throw new PendingException();
	Assert.assertTrue(driver.findElement(By.linkText(lin)).isDisplayed());
	driver.close();
	
}
}
