package stepDefenetions;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.Assert;

import cucumber.api.PendingException;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class LoginLogout {
	WebDriver driver = new FirefoxDriver();
	@Given("^i am in the new tours login page\"(.*?)\"$")
	public void i_am_in_the_new_tours_login_page(String url) throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	    driver.get(url);
		//throw new PendingException();
	}
	@When("^i enter username \"(.*?)\" and password \"(.*?)\"$")
	public void i_enter_username_and_password(String username, String password) throws Throwable {
	   // Write code here that turns the phrase above into concrete actions
	    driver.findElement(By.name("userName")).sendKeys(username);
	    driver.findElement(By.name("password")).sendKeys(password);
		//throw new PendingException();
	}
	@When("^i press Signin button$")
	public void i_press_Signin_button() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
		driver.findElement(By.name("login")).click();
	   // throw new PendingException();
	}

	@Then("^login should be successful and \"(.*?)\" link should be displayed$")
	public void login_should_be_successful_and_link_should_be_displayed(String signoff) throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
		Assert.assertTrue(driver.findElement(By.linkText(signoff)).isDisplayed());
	    //throw new PendingException();
	}

}
