#Author: your.email@your.domain.com
#Keywords Summary :
#Feature: List of scenarios.
#Scenario: Business rule through list of steps with arguments.
#Given: Some precondition step
#When: Some key actions
#Then: To observe outcomes or validation
#And,But: To enumerate more Given,When,Then steps
#Scenario Outline: List of steps for data-driven as an Examples and <placeholder>
#Examples: Container for s table
#Background: List of steps run before each of the scenarios
#""" (Doc Strings)
#| (Data Tables)
#@ (Tags/Labels):To group Scenarios 
#<> (placeholder)
#""
## (Comments)

#Sample Feature Definition Template
@tag
Feature: test Page

@tag1
Scenario: checking the page with correct credentials 
Given I  am in the test  main page"http://www.training.qaonlinetraining.com/testPage.php"
When I enter Name"sinan" and email"sinantalawsi@gmail.com" 
 Then I enter website"www.google.com" and comment "how are you"
	And i check  "gender" within "male"
	And i uncheck "what do you have" within "car"
Then I check "what do you have" within"bike" the outcomes
	And i check "country" within "India"
	Then i select "Skill" within "Database"
	And i click "send" 
	Then input should contain "INDIA"

#@tag2
#Scenario Outline: Title of your scenario outline
#Given I want to write a step with <name>
#When I check for the <value> in step
#Then I verify the <status> in step

#Examples:
 #   | name  |value | status |
  #  | name1 |  5   | success|
   # | name2 |  7   | Fail   |
